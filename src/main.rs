#[macro_use]
extern crate clap;
extern crate gitlab;
use clap::{App, SubCommand, Arg, AppSettings, ArgMatches};

const PROGNAME: &'static str = "glc";

fn main() {

    let matches = App::new(PROGNAME)
        .bin_name(PROGNAME)
        .author(crate_authors!())
        .version(crate_version!())
        .about("GitLab command line client")
        .setting(AppSettings::GlobalVersion)
        .setting(AppSettings::VersionlessSubcommands)
        .setting(AppSettings::SubcommandRequiredElseHelp)
        .arg(Arg::with_name("verbose")
            .short("v")
            .long("verbose")
            .help("Show more details"))
        .subcommand(SubCommand::with_name("info"))
        .subcommand(SubCommand::with_name("project")
            .setting(AppSettings::ArgRequiredElseHelp)
            .setting(AppSettings::TrailingVarArg)
            .arg_from_usage("<record>... 'password record to show'"))
        .get_matches();

    println!("{:?}", matches);
}
